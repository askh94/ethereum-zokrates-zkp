var express = require('express');
var router = express.Router();

var setupController = require('../../controllers/setup.controller');

router.post('/compilePreimageFunction', setupController.compileZokratesPreImageFunction);

router.post('/generateVerificationKey', setupController.generateKeys);

router.post('/generateVerifierContract', setupController.generateSmartContract);

router.post('/deployVerifierSmartContract', setupController.deploySmartContract);

router.post('/registerIssuer', setupController.registerIssuer);

module.exports = router;