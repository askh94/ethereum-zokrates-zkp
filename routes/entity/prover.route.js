var express = require('express');
var router = express.Router();
var proverController = require('../../controllers/prover.controller');

router.post('/compilePreimageFunction', proverController.compileZokratesPreImageFunction);

router.post('/compileHashFunction', proverController.compileZoKratesHashFunction);

router.post('/generateHash', proverController.generateHash);

router.post('/generateProof', proverController.generateProof);

router.post('/verifyProof', proverController.verifyProof);


module.exports = router;