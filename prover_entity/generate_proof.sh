#!/usr/bin/env bash
set -e
POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -q|--signature1)
    SIGNATURE1="$2"
    shift # past value
    shift
    ;;
    -t|--signature2)
    SIGNATURE2="$2"
    shift # past argument
    shift
    ;;
    -s|--secret)
    SECRET="$2"
    shift # past argument
    shift
    ;;
    --default)
    DEFAULT=YES
    shift # past argument
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

if [[ -n $1 ]]; then
    echo "Last line of file specified as non-opt/last argument:"
    tail -1 "$1"
fi

if [ -z "$SECRET" ]; then
  echo "No secret was set!"
  exit 0
fi

if [ -z "$SIGNATURE1" ]; then
  echo "Signature incomplete!"
  exit 0
fi

if [ -z "$SIGNATURE2" ]; then
  echo "Signature incomplete!"
  exit 0
fi

./zokrates compute-witness --light -a $SIGNATURE1 $SIGNATURE2 $SECRET -i out_preimage -o witness_secret
./zokrates generate-proof -i out_preimage -j proof.json -p ./proving.key -w witness_secret 
echo GENERATED PROOF-FILE