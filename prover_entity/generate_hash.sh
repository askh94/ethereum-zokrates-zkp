#!/usr/bin/env bash
set -e
POSITIONAL=()
while [[ $# -gt 0 ]]
do
key="$1"

case $key in
    -s|--secret)
    SECRET="$2"
    shift # past argument
    shift # past value
    ;;
	-c|--filecounter)
    FILECOUNTER="$2"
    shift # past value
    shift
    ;;
    --default)
    DEFAULT=YES
    shift # past argument
    ;;
    *)    # unknown option
    POSITIONAL+=("$1") # save it in an array for later
    shift # past argument
    ;;
esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

if [[ -n $1 ]]; then
    echo "Last line of file specified as non-opt/last argument:"
    tail -1 "$1"
fi

if [ -z "$SECRET" ]; then
  echo "No secret was set!"
  exit 0
fi

touch preimage_hash
./zokrates compute-witness --light -a $SECRET -i out_hash
