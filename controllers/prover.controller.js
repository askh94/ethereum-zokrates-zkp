var exec = require('child_process').exec;
var util = require('util')
const fs = require('fs');


var web3 = require('web3');
const config = require("../config/config.json");
web3 = new web3(new web3.providers.WebsocketProvider(config.web3config.websocket));
web3.eth.defaultAccount = config.admin_account_address;

const storage = require('node-persist');
storage.init();

module.exports.compileZokratesPreImageFunction = function (req, res) {
    var command = "bash ./prover_entity/compile_preimage_function.sh";
    exec(command, function (err, stdout, stderr) {
        console.log(stdout.toString('utf8'));
    });
    res.send('done');
}
module.exports.compileZoKratesHashFunction = function (req, res) {
    var command = "bash ./prover_entity/compile_hash_function.sh";
    exec(command, function (err, stdout, stderr) {
        console.log(stdout.toString('utf8'));
    });
    res.send('done');
}

module.exports.generateHash = async function (req, res, next) {
    try {
        var secret = req.query.secret;
        var command = util.format('bash ./prover_entity/generate_hash.sh -s %d', secret);
        exec(command, async function (err, stdout, stderr) {
            var HashOutputArray = stdout.split("\n");
            var matchedOutput = new Array();
            var secretHashArray = new Array();
            var regex = /\d+/g
            for (var item of HashOutputArray) {
                matchedOutput.push(item.match(regex));
            }
            var out_0 = matchedOutput[4][0];
            var out_1 = matchedOutput[4][1];
            secretHashArray.push(out_0);
            secretHashArray.push(out_1);
            await storage.setItem('hashArray', secretHashArray);
            await storage.setItem('chosenSecret', secret);
            res.send('done');
        });
    } catch (error) {
        next(error);
    }
}
module.exports.generateProof = async function (req, res, next) {
    try {
        var userSecretHashArray = await storage.getItem('hashArray');
        var chosenSecret = await storage.getItem('chosenSecret');
        var command = util.format('bash ./prover_entity/generate_proof.sh -s %d -q %o -t %o', chosenSecret, userSecretHashArray[0], userSecretHashArray[1]);
        exec(command, function (err, stdout, stderr) {
            console.log(stdout);
            res.send('done');
        });
    } catch (error) {
        next(error);
    }
}
module.exports.verifyProof = async function (req, res, next) {
    try {
        var verifierContract = new web3.eth.Contract(config.contract_abis.verifier_contract_abi, config.contract_addresses.verifier_contract_address, {
            from: web3.eth.defaultAccount
        });
        let rawdata = fs.readFileSync('proof.json');
        let proofData = JSON.parse(rawdata);
        var receipt = await verifierContract.methods.verifyTx(proofData.proof.a, proofData.proof.b, proofData.proof.c, proofData.inputs).send({
            gas: '4000000'
        })
        if (receipt.events != null) {
            res.send(receipt.events.Verified.returnValues.s);
        }
    } catch (error) {
        next(error);
    }
}