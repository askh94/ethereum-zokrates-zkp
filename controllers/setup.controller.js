var exec = require('child_process').exec;
var web3 = require('web3');
const config = require("../config/config.json");
web3 = new web3(new web3.providers.WebsocketProvider(config.web3config.websocket));
web3.eth.defaultAccount = config.admin_account_address;


module.exports.compileZokratesPreImageFunction = function (req, res) {
  var command = "bash ./secure_entity/compile_preimage_function.sh";
  exec(command, function (err, stdout, stderr) {
    console.log(stdout.toString('utf8'));
  });
  res.send('done');
}

module.exports.generateKeys = function (req, res) {
  var command = "bash ./secure_entity/generate_verification_key.sh";
  exec(command, function (err, stdout, stderr) {
    console.log(stdout.toString('utf8'));
  });
  res.send('done');
}
module.exports.generateSmartContract = function (req, res) {
  var command = "bash ./secure_entity/generate_verifier_smart_contract.sh";
  exec(command, function (err, stdout, stderr) {
    console.log(stdout.toString('utf8'));
  });
  res.send('done');
}
module.exports.deploySmartContract = function (req, res) {
  var command = "bash ./truffle/deploy_verifier_smart_contract.sh";
  exec(command, function (err, stdout, stderr) {
    console.log(stdout.toString('utf8'));
  });
  res.send('done');
}
module.exports.registerIssuer = async function (req, res, next) {
  try {
    var signedAttributesContract = new web3.eth.Contract(config.contract_abis.signedattributes_contract_abi, config.contract_addresses.signedattributes_contract_address, {
      from: web3.eth.defaultAccount
    });

    var issuers = new Array();
    var registeredEventResponse = new Array();
    issuers.push(config.issuer_addresses.kba_address);
    issuers.push(config.issuer_addresses.bmw_address);

    for (var address of issuers) {
      receipt = await signedAttributesContract.methods.registerIssuer(address).send();
      registeredEventResponse.push(receipt.events)
    }


    for (response of registeredEventResponse) {
      if (response.Registered.returnValues.registeredSuccessfully && !response.Registered.returnValues.alreadyRegistered) {
        res.send('issuers registered successfully');
      } else if (!response.Registered.returnValues.registeredSuccessfully && response.Registered.returnValues.alreadyRegistered) {
        res.send('issuers already registered');

      }
    }
  } catch (error) {
    next(error);
  }
}