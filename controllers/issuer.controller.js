var exec = require('child_process').exec;
var web3 = require('web3');
const config = require("../config/config.json");
web3 = new web3(new web3.providers.WebsocketProvider(config.web3config.websocket));


const storage = require('node-persist');
storage.init();

module.exports.issueAttributes = async function (req, res, next) {
    var proverAddress = req.query.address
    var name = req.query.attributeName;
    var value = req.query.attribueValue;
    var selectedIssuer = req.query.selectedIssuer;
    try {
        if (selectedIssuer == "kba") {
            var signedAttributesContract = new web3.eth.Contract(config.contract_abis.signedattributes_contract_abi, config.contract_addresses.signedattributes_contract_address, {
                from: config.issuer_addresses.kba_address
            });
        } else {
            var signedAttributesContract = new web3.eth.Contract(config.contract_abis.signedattributes_contract_abi, config.contract_addresses.signedattributes_contract_address, {
                from: config.issuer_addresses.bmw_address
            });
        }
        var userSecretHashArray = await storage.getItem('hashArray');
        var receipt = await signedAttributesContract.methods.setSignedAttributes(proverAddress, name, value, userSecretHashArray[0], userSecretHashArray[1]).send({
            gas: '4000000'
        });
        if (receipt.events != null) {
            res.send(receipt.events.AttributeAdded.returnValues.attributeAdded);
        }
    } catch (error) {
        next(error);
    }

}