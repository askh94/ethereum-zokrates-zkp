const Verifier = artifacts.require("Verifier");
const BN256G2 = artifacts.require("BN256G2"); 
const SignedAttributes = artifacts.require("SignedAttributes");
let = deploymentAddress = "0xCDd5E5c9321f4572f8E816fd88619657CEE1E8b1";
module.exports = async function(deployer, network, accounts) {
    await deployer.deploy(BN256G2, {from: deploymentAddress});
    await deployer.link(BN256G2, Verifier, {from: deploymentAddress});
    await deployer.deploy(Verifier,{from: deploymentAddress});
    await deployer.deploy(SignedAttributes,{from: deploymentAddress});

}