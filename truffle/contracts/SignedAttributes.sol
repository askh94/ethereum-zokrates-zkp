pragma solidity >=0.5.0 < 0.6.0;

import "./Ownable.sol";

contract SignedAttributes is Ownable{

    struct SignedAttribute {
        address owner;
        string name;
        string value;
        Signature signature;
    }

    struct Signature {
        uint256 sign1;
        uint256 sign2;
    }

    mapping(bytes32 => SignedAttribute) private signedAttributes;
    address[] issuers;
    string[] attributeNames;

    event Registered(address issuer, bool registeredSuccessfully, bool alreadyRegistered);
    event AttributeAdded(address owner, string attributeName,string attributeValue,bool attributeAdded);

    constructor() public {

    }

    modifier onlyIssuer() {
        bool isIssuerPresent = false;
        for(uint i; i<issuers.length; i++) {
            if(msg.sender == issuers[i]) {
                isIssuerPresent = true;
                break;
            }
        }
        require(isIssuerPresent);
        _;
    }

    modifier onlyOwner() {
        require(msg.sender == owner());
        _;
    }

    /**
     * Only each issuer once --> change to mapping?
     */
    function registerIssuer(address issuer) onlyOwner external returns(bool) {
        bool isIssuerPresent = false;
        for (uint i; i < issuers.length; i++) {
            address currentIssuer = issuers[i];
            if (currentIssuer == issuer) {
                isIssuerPresent = true;
            }
        }
        if (!isIssuerPresent) {
            issuers.push(issuer);
            emit Registered(issuer, true, false);
        } else {
            emit Registered(issuer, false, true);
        }
        return isIssuerPresent;
    }

    function setSignedAttributes(address owner, string calldata attributeName, string calldata attributeValue, uint256 sign1, uint256 sign2) onlyIssuer external {
        SignedAttribute memory newAttribute = SignedAttribute(owner, attributeName, attributeValue, Signature(sign1, sign2));
        bytes32 mappingKey = keccak256(abi.encodePacked(sign1, sign2));
        signedAttributes[mappingKey] = newAttribute;
        bool isAttributePresent = false;
        for (uint i; i < attributeNames.length; i++) {
            string memory currentName = attributeNames[i];
            if (keccak256(abi.encodePacked(currentName)) == keccak256(abi.encodePacked(attributeName))) {
                isAttributePresent = true;
            }
        }
        if (!isAttributePresent) {
            attributeNames.push(attributeName);
        }
        emit AttributeAdded(owner, attributeName, attributeValue, true);
    }

    /**
     * change regarding mapping instead of array
     */
    function getAttribute(uint256 sign1, uint256 sign2) external view returns(address, string memory, string memory) {
        bytes32 mappingKey = keccak256(abi.encodePacked(sign1, sign2));
        SignedAttribute memory currentAttribute = signedAttributes[mappingKey];
        return (currentAttribute.owner, currentAttribute.name, currentAttribute.value);
    }

    function getIssuers() external view returns(address[] memory) {
        return issuers;
    }
}